
$(document).ready(function(){              

	var pathname = window.location.pathname;
	var defaultt = '/personalagenda.ro/public/';
	
	var applications = "/personalagenda.ro/public/applications";
	var create_new_application = "/personalagenda.ro/public/create_new_application";
	var edit_application = "/personalagenda.ro/public/edit_applications";
	
	var notes = "/personalagenda.ro/public/notes";
	var create_new_note = "/personalagenda.ro/public/create_new_note";
	var edit_notes = "/personalagenda.ro/public/edit_notes";
	

	if (pathname == defaultt) {
		$("#applications").addClass('active');
	};

// APPLICATIONS
	if (pathname == applications) {
		$("#applications").addClass('active');
	};

	if (pathname == create_new_application) {
		$("#create_new_application").addClass('active');
	};

	if (pathname == edit_application) {
		$("#edit_application").addClass('active');
		$(".hidden_menu").removeClass('hidden');
	};

// NOTES
	if (pathname == notes) {
		$("#notes").addClass('active');
	};

	if (pathname == create_new_note) {
		$("#create_new_note").addClass('active');
	};

	if (pathname == edit_notes) {
		$("#edit_note").addClass('active');
		$(".hidden_menu").removeClass('hidden');
	};

});

$(document).ready(function(){              
	$("#maintenance").click(function(){
		if ($(".hidden_menu").hasClass("hidden")) {
			$(".hidden_menu").removeClass("hidden");
		} else {
			$(".hidden_menu").addClass("hidden");
		};
		
	})
});

// check all checkboxes when #check_all is checked
$(document).ready(function(){
	$("#check_all").change(function(){
		if ($(this).is(":checked"))
			{
				$(".checkboxx").prop('checked', true);
			} else {
				$(".checkboxx").prop('checked', false);
			}	 		
	})
})

// check #check_all when all the checkboxes are selected and uncheck #check_all when not all checkboxes selected
$(document).ready(function(){	
	$(".checkboxx").change(function(){
		if ($(".checkboxx").length == $(".checkboxx:checked").length) {
			$("#check_all").prop('checked', true);
		} else {
			$("#check_all").prop('checked', false);
		};	
	})	
})
