<?php
//hi
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::get('/', function()
// {

// 	if (Auth::check()) {
// 		return View::make('applications');
// 	} else  {
// 				return View::make('homepage');
// 			}
// });


// USERS

Route::get('create_new_user', function()
{
	return View::make('create_new_user_page');
});
// Route::get('create_new_user', 'UsersController@generateNewUser');
Route::post('generate_new_user', array('uses' => 'UsersController@generateNewUser',
                                        'as' => 'create.new_user'));

// APPLICATIONS
Route::get('/', 'ApplicationsController@showApplications');

Route::get('applications', 'ApplicationsController@showApplications');
Route::get('create_new_application', 'ApplicationsController@createNewApplication');
Route::get('edit_applications', 'ApplicationsController@editApplications');

Route::get('delete_application_with_id/{id}', array('as' => 'app_id_to_delete', 'uses' => 'ApplicationsController@deleteApplication'));
Route::get('edit_made_application/{id}', array('as' => 'app_id_to_edit', 'uses' => 'ApplicationsController@editApplication'));


Route::post('generate_new_application', array('uses' => 'ApplicationsController@generateNewApplication',
                                        'as' => 'generate.new_application'));

Route::post('delete_selected_applications', array('uses' => 'ApplicationsController@delSelApp',
                                        'as' => 'delete.selected_applications'));

Route::post('edit_selected_applications', array('uses' => 'ApplicationsController@editSelApp',
                                        'as' => 'edit.new_application'));


// NOTES
Route::get('notes', 'NotesController@showNotes');
Route::get('create_new_note', 'NotesController@createNewNote');
Route::get('edit_notes', 'NotesController@editNotes');

Route::get('delete_note_with_id/{id}', array('as' => 'note_id_to_delete', 'uses' => 'NotesController@deleteNote'));
Route::get('edit_made_note/{id}', array('as' => 'note_id_to_edit', 'uses' => 'NotesController@editNote'));


Route::post('generate_new_note', array('uses' => 'NotesController@generateNewNote',
                                        'as' => 'generate.new_note'));
Route::post('delete_selected_notes', array('uses' => 'NotesController@deleteSelectedNotes',
                                        'as' => 'delete.selected_notes'));
Route::post('edit_selected_notes', array('uses' => 'NotesController@editSelNote',
                                        'as' => 'edit.new_note'));



Route::get('login', 'SessionsController@create');
Route::get('logout', 'SessionsController@destroy');
Route::resource('sessions', 'SessionsController');



Route::get('profile', function()
{
	if (Auth::check()) {
		return "Welcome" . " " . Auth::user()->toJson();
	} else {
		return View::make('homepage');
	}

});

Route::get('homepage', function()
{
	return View::make('homepage');
});


