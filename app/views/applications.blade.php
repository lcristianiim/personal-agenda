@extends('layouts.master_page_child')

@section('title')
  PersonalAgenda
@stop

@section('navigation_bar')
   <div role="navigation" class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="homepage" class="navbar-brand">Personal Agenda</a>
        </div>
        
        <div class="navbar-collapse collapse">
            
            <ul class="nav navbar-nav navbar-right">            
             <li class="dropdown">
               <a data-toggle="dropdown" class="dropdown-toggle" href="#">{{Auth::user()->email}} <b class="caret"></b></a>
               <ul class="dropdown-menu">
                 <li><a href="logout">logout</a></li>
               </ul>
             </li>
            </ul>

        </div><!--/.navbar-collapse -->
      </div>
   </div>
@stop

@section('jumbotron') 
<div class="row">
  
  <div class="col-sm-3 col-md-2 sidebar">
    @yield('first_menu')          
    @yield('secondary_menu')
  </div>

  @yield('main_content')       
</div>
  
@stop

@section('activities')
  
@stop