@extends('applications')

@section ('first_menu')

  @include('includes.first_menu')

@stop

@section('secondary_menu')
  @include('includes.secondary_menu')
@stop

@section('main_content')
  
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">           
   <h2 class="sub-header">Create a new Application</h2>
      <div class="container">
         <div class="col-sm-6">
            
               
               {{ Form::open(array('action' => 'generate.new_application', 'role' => 'form', 'class' => 'form-horizontal' )) }}
            
               <div class="form-group">              
                 <label class="col-sm-2 control-label" >Application</label>
                 <div class="col-sm-10">
                     {{ Form::text('application_name', null, array('class' => 'form-control', 'placeholder' => 'Name of the application')) }}
                     <?php echo $errors->first('application_name'); ?>
                 </div>
                

               </div>

               <div class="form-group">              
                 <label class="col-sm-2 control-label" >Username</label>
                 <div class="col-sm-10">
                     {{ Form::text('username_for_application', null, array('class' => 'form-control', 'placeholder' => 'Username'))}}
                     <?php echo $errors->first('username_for_application'); ?>
                 </div>
               </div>

               <div class="form-group">              
                 <label class="col-sm-2 control-label" >Password</label>
                 <div class="col-sm-10">
                     {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password'))}}
                     <?php echo $errors->first('password'); ?>
                 </div>
               </div>

               <div class="form-group">              
                 <label class="col-sm-2 control-label" >Confirm password</label>
                 <div class="col-sm-10">
                     {{ Form::password('confirm_password', array('class' => 'form-control', 'placeholder' => 'Confirm password'))}}
                     <?php echo $errors->first('confirm_password'); ?>
                 </div>
               </div>

               <div class="form-group">              
                 <label class="col-sm-2 control-label" for="inputEmail3">Details</label>
                 <div class="col-sm-10">
                     {{ Form::textarea('details', null, array('class' => 'form-control', 'rows' => '3', 'placeholder' => 'Specific details'))}}
                 </div>
               </div>

               <div class="form-group">
                 <div class="col-sm-offset-2 col-sm-10">
                     {{ Form::submit('Create', array('class' => 'btn btn-default')) }}
                 </div>
               </div>

               {{ Form::close() }}

            
         </div>
      </div>
</div>
@stop