@extends('applications')

@section ('first_menu')

	@include('includes.first_menu')

@stop

@section('secondary_menu')
	@include('includes.secondary_menu')
@stop

@section('main_content')
	
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">           
		<h2 class="sub-header">Applications</h2>
		<div class="table-responsive">
			<table class="table table-striped">

				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Username</th>
						<th>Password</th>
						<th>Details</th>
					</tr>
				</thead>

				<tbody>
					<?php $i = 0; ?>
					@foreach ($applications_by_auth_user as $abau)
						<?php $i = $i+1; ?>
						<tr>
							<td> {{$i}} </td>
							<td> {{ Crypt::decrypt($abau->name) }}</td>
							<td> {{ Crypt::decrypt($abau->username) }} </td>
							<td> {{ Crypt::decrypt($abau->password) }} </td>
							<td> {{ $abau->details }} </td>
						</tr>
					@endforeach
				</tbody>

			</table>
		</div>
	</div>
@stop