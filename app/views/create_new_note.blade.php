@extends('applications')

@section ('first_menu')

  @include('includes.first_menu')

@stop

@section('secondary_menu')
  @include('includes.secondary_menu')
@stop

@section('main_content')
  
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">           
   <h2 class="sub-header">Create a new Note</h2>
      <div class="container">
         <div class="col-sm-6">            
               
               {{ Form::open(array('action' => 'generate.new_note', 'role' => 'form', 'class' => 'form-horizontal' )) }}
            
               <div class="form-group">              
                 <label class="col-sm-2 control-label" >Title</label>
                 <div class="col-sm-10">
                     {{ Form::text('title', null, array('class' => 'form-control', 'placeholder' => 'Title')) }}
                     <?php echo $errors->first('title'); ?>
                 </div>
               </div>


               <div class="form-group">              
                 <label class="col-sm-2 control-label" for="inputEmail3">Note</label>
                 <div class="col-sm-10">
                     {{ Form::textarea('text', null, array('class' => 'form-control', 'rows' => '10', 'placeholder' => 'Notes..'))}}
                 </div>
               </div>

               <div class="form-group">
                 <div class="col-sm-offset-2 col-sm-10">
                     {{ Form::submit('Create', array('class' => 'btn btn-default')) }}
                 </div>
               </div>

               {{ Form::close() }}
            
         </div>
      </div>
</div>
@stop