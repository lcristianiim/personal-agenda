<html>
<head>
	<title>Create a new user</title>
	<link rel="stylesheet" type="text/css" href="<?= asset('bootstrap-3.1.0') ?>/dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?= asset('css') ?>/create_new_user.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('Font-Awesome-master') ?>/css/font-awesome.min.css">   
    

</head>
<body>
	<div class="container">
	{{ Form::open(array('action' => 'create.new_user', 'role' => 'form', 'class' => 'form-horizontal', 'class' => 'form-signin' )) }}
    
    	<h2 class="form-signin-heading">Create a new user</h2>
	 {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Email adress', 'name' => 'email', 'required' => '')) }}        

	    <div class="controls">
		    <input type="password" name="password" required="" class="form-control" placeholder="Password" />
		    <p class="help-block"></p>
	    </div>
	    
	    
	    <div class="controls">
	    	<input type="password" name="confirm_password" class="form-control" placeholder="Confirm password">
	    	<?php echo $errors->first('confirm_password'); ?>
	    </div>
	    
	    <button type="submit" class="btn btn-lg btn-primary btn-block" >Create</button>	    
    </form>

    </div>
</body>
</html>