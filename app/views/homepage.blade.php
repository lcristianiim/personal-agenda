@extends('layouts.master_page_child')

@section('title')
  PersonalAgenda
@stop

@section('navigation_bar')
	<div role="navigation" class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="#" class="navbar-brand">Personal Agenda</a>
        </div>
        
        <div class="navbar-collapse collapse">
          
          @if(Auth::check())
             <ul class="nav navbar-nav navbar-right">            
             <li class="dropdown">
               <a data-toggle="dropdown" class="dropdown-toggle" href="#">{{Auth::user()->email}} <b class="caret"></b></a>
               <ul class="dropdown-menu">
                 <li><a href="logout">logout</a></li>
               </ul>
             </li>
            </ul>
          @else
            {{ Form::open(array('route' => 'sessions.store', 'role' => 'form', 'class' => 'navbar-form navbar-right' )) }}
            
            <div class="form-group">              
              {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Email')) }}
            </div>

            <div class="form-group">              
              {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password'))}}
            </div>
            
          {{ Form::submit('Sign in', array('class' => 'btn btn-success')) }}

          <a href="create_new_user"> <input class ="btn btn-success" type="button" value ="Create new user" action="test"> </a> 

          {{ Form::close() }}
          @endif
          


        </div><!--/.navbar-collapse -->
      </div>
    </div>
@stop

@section('jumbotron')
	<div class="jumbotron">
      <div class="container">
        <h1>Welcome</h1>
        <p>Organize youre mind, and after that, the world around you.</p>        
      </div>
    </div>
@stop

@section('activities')
	<div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
          <h2>Applications</h2>
          <a href="applications">
			<i class="fa fa-key thumbnail" style="font-size: 15em;"></i>
          </a>
          <p>Store all the details of loggin to youre applications.</p>          
        </div>
        
        <div class="col-md-4">
          <h2>Notes</h2>
          <a href="notes">
			<i class="fa fa-book thumbnail" style="font-size: 15em;"></i>
          </a>
          <p>Write here all the data you dont want to forget.</p>
          
       </div>
        <div class="col-md-4">
          <h2>Records</h2>
          <a href="records">
			<i class="fa fa-calendar thumbnail" style="font-size: 15em;"></i>
          </a>
          <p>Record youre activity to keep track of youre actions.</p>
          
        </div>
      </div>

      <hr>

      <footer>
        <p>&copy; Personal Agenda 2014</p>
      </footer>
    </div>
@stop