@extends('applications')

@section ('first_menu')

	@include('includes.first_menu')

@stop

@section('secondary_menu')
	@include('includes.secondary_menu')
@stop

@section('main_content')
	
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">           
		<h2 class="sub-header">Notes</h2>
		<div class="table-responsive">
			<table class="table table-striped">

				<thead>
					<tr>
						<th>#</th>
						<th>title</th>
						<th>notes</th>						
					</tr>
				</thead>

				<tbody>					
					<?php $i = 0; ?>
					@foreach ($notes_by_auth_user as $nbau)
						<?php $i = $i+1 ?>
						<tr>
							<td> {{$i}} </td>
							<td> {{$nbau->title}}</td>
							<td> {{$nbau->text}}</td>
						</tr>
					@endforeach
				</tbody>

			</table>
		</div>
	</div>
@stop