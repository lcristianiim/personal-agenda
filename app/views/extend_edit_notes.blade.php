@extends('applications')

@section ('first_menu')

	@include('includes.first_menu')

@stop

@section('secondary_menu')
	@include('includes.secondary_menu')
@stop

@section('main_content')
	
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">           
		<h2 class="sub-header">Applications Maintenance</h2>
		<div class="table-responsive">
			
			{{ Form::open(array('action' => 'delete.selected_notes', 'id' => 'note_to_del_form')) }}

			<table class="table table-striped">

				<thead>
					<tr>
						<th>#</th>
						<th>Options</th>						
						<th>Notes</th>
						<th>Created at</th>
						<th>Modified at</th>
					</tr>
				</thead>

				<tbody>
					
					<?php $i = 0; ?>
					@foreach ($notes_by_auth_user as $nbau)
						<?php $i = $i+1; ?>						
						<tr>
							<td> {{$i}} </td>
							<td>
								{{Form::checkbox($nbau->title, $nbau->id, null, ['class' => 'checkboxx'] ) }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<?php
									echo "<a href=\"edit_made_note/" . $nbau->id . "\"" . "><i class=\"fa fa-pencil fa-fw\"></i> Edit</a>";?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<?php
									echo "<a href=\"delete_note_with_id/" . $nbau->id . "\"" . "><i class=\"fa fa-trash-o fa-fw red\"></i> Delete</a>";
								 ?>
							</td>
							<td> {{ $nbau->title }}</td>
							<td> {{ $nbau->created_at }} </td>
							<td> {{ $nbau->updated_at }} </td>
						</tr>
					@endforeach
				</tbody>		

			</table>
			
			<div class="edit_options container-fluid">
				<div class="row">
					<div class="col-sm-10">
						<img src="images/arrow_ltr.png">
						<input id="check_all" type="checkbox" value="select all"> Check all &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						With selected: <a href="#">&nbsp;						
						<a href="javascript:{}" onclick="document.getElementById('note_to_del_form').submit(); return false;"><i class="fa fa-trash-o fa-fw red"></i> Delete</a>
					</div>
				</div>
			</div>

			{{Form::close()}}

		</div>
	</div>
@stop