@extends('applications')

@section ('first_menu')

  @include('includes.first_menu')

@stop

@section('secondary_menu')
  @include('includes.secondary_menu')
@stop

@section('main_content')
  
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">           
   <h2 class="sub-header">Edit Note</h2>
      <div class="container">
         <div class="col-sm-6">
            
               
               {{ Form::open(array('action' => 'edit.new_note', 'role' => 'form', 'class' => 'form-horizontal' )) }}
            
               <div class="form-group">              
                 <!-- hidden input -->
                 <label class="col-sm-2 control-label" >Title</label>
                 {{ Form::text('note_id', $note->id, array('class' => 'hidden', 'placeholder' => 'id')) }}
                 
                 <div class="col-sm-10">                  
                     {{ Form::text('note_title', $note->title, array('class' => 'form-control', 'placeholder' => 'Title')) }}
                     <?php echo $errors->first('application_name'); ?>
                 </div>                

               </div>

               <div class="form-group">              
                 <label class="col-sm-2 control-label" >Notes</label>
                 <div class="col-sm-10">
                     {{ Form::textarea('details', $note->text, array('class' => 'form-control', 'placeholder' => 'Notes..', 'rows' => '10'))}}
                     <?php echo $errors->first('username_for_application'); ?>
                 </div>
               </div>               

               <div class="form-group">
                 <div class="col-sm-offset-2 col-sm-10">
                     {{ Form::submit('Edit this Note', array('class' => 'btn btn-default')) }}
                 </div>
               </div>

               {{ Form::close() }}

            
         </div>
      </div>
</div>
@stop