@extends('layouts.master_page')

@section ('title')
	@yield('title')
@stop

@section ('header')
	
@stop

@section ('content')
	@yield('navigation_bar')
	@yield('jumbotron')
	@yield('activities')
@stop

@section ('footer')
@stop