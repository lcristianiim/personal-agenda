<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    @yield('head')
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?= asset('bootstrap-3.1.0') ?>/dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('Font-Awesome-master') ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('css') ?>/body_css.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('css') ?>/dashboard.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('css') ?>/custom.css">    
    <script src="<?= asset('js') ?>/jquery.min.js" type="text/javascript"></script>
    <script src="<?= asset('js') ?>/custom_scripts.js" type="text/javascript"></script>

    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    @yield('content')

    @yield('footer')

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>    
  </body>
</html>
