<?php 
	/**
	* 
	*/
	class SessionsController extends BaseController
	{
		public function index()
		{
			return View::make('sessions.index');
		}

		public function create()
		{
			return View::make('sessions.create');
		}

		public function store()
		{
			// taking credentials
			$input = Input::all();
			
			// autentification atempt
			$attempt = Auth::attempt([
				'email' => $input['email'],
				'password' => $input['password']
			]);

			if ($attempt) {							
				return Redirect::to('/');
			}			
		}
		public function destroy()
		{
			Auth::logout();
			return Redirect::to('/');
		}
		
	}
 ?>