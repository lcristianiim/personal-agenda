<?php 
	/**
	* extends basecontroller
	*/
	class UsersController extends BaseController
	{
		public function generateNewUser() {
			$input = Input::all();

			$rules = array(			
			'confirm_password' => 'required|same:password'
			);

			$validation = Validator::make($input, $rules);
			$messages = $validation->messages();

			if ($validation->fails())
			{
			    return Redirect::to('create_new_user')->withErrors($validation)->withInput();
			} else {
				// dd($input);
				$user = new User;
				$user->email = $input['email'];
				$user->password = Hash::make($input['password']);
				$user->save();

				return Redirect::to('homepage');

			};
		}
	}
 ?>