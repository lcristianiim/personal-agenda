<?php
class NotesController extends BaseController{

    public function showNotes(){
        if (Auth::check()) {
            $notes = DB::table('notes')->get();
            $enrollments = DB::table('enrollments')->where('user_id', Auth::user()->id)->get();
            $notes_by_auth_user = array();

            foreach ($enrollments as $enrollment) {
                foreach ($notes as $note) {
                    if ($enrollment->notes_id == $note->id) {
                        $notes_by_auth_user[] = $note;
                    }
                }
            }
        }

        if (Auth::check()) {
            return View::make('extend_notes')->with('notes_by_auth_user', $notes_by_auth_user);
        } else {
            return View::make('homepage');
        }
    }

    public function createNewNote(){
        return View::make('create_new_note');
    }

    public function generateNewNote(){

        $input = Input::all();
        $rules = array('title' => 'required');

        $validation = Validator::make($input, $rules);
        $messages = $validation->messages();

        if ($validation->fails())
        {
            return Redirect::to('create_new_note')->withErrors($validation)->withInput();
        } else {
            // create a record in "note" database
            $note = new Note;
            $note->title = $input['title'];
            if (isset($input['text'])) {
                $note->text = $input['text'];
            }
            $note->save();

            // create a record in "enrollments" database
            $enrollment = new Enrollment;
            $enrollment->user_id = Auth::user()->id;
            $enrollment->notes_id = $note->id;
            $enrollment->save();

            return Redirect::to('notes');
        }


    }

    public function editNotes()
    {
        if (Auth::check()) {
            $notes = DB::table('notes')->get();
            $enrollments = DB::table('enrollments')->where('user_id', Auth::user()->id)->get();
            $notes_by_auth_user = array();

            foreach ($enrollments as $enrollment) {
                foreach ($notes as $note) {
                    if ($enrollment->notes_id == $note->id) {
                        $notes_by_auth_user[] = $note;
                    }
                }
            }
        }

        if (Auth::check()) {
            return View::make('extend_edit_notes')->with('notes_by_auth_user', $notes_by_auth_user);
        } else {
            return View::make('homepage');
        }
    }

    public function deleteNote($note_id_to_delete){
        // dd($note_id_to_delete);
        $note = Note::find($note_id_to_delete);
        // dd($note);
        $note->delete();

        $enrollment = DB::table('enrollments')->where('notes_id', $note_id_to_delete);
        $enrollment->delete();

        return Redirect::to("edit_notes");
    }

    public function deleteSelectedNotes(){
        $checked_notes = Input::all();


        foreach ($checked_notes as $name => $note_id) {

            $note = Note::find($note_id);
            if ($note != null) {
                $note->delete();
                $enrollment = DB::table('enrollments')->where('notes_id', $note_id);
                $enrollment->delete();
            }
        }
        return Redirect::to("edit_notes");
    }

    public function editNote($note_id_to_edit){

        $note = Note::find($note_id_to_edit);
        return View::make("edit_made_note")->with('note', $note);
    }

    public function editSelNote(){
        $input = Input::all();
        // dd($input);
        $note = Note::find($input['note_id']);
        // dd($application);

        $note->title = $input['note_title'];
        $note->text = $input['details'];

        $note->save();

        return Redirect::to("edit_notes");
    }
}
?>
