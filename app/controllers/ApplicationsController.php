<?php

class ApplicationsController extends BaseController {

        /*
        |--------------------------------------------------------------------------
        | Default Home Controller
        |--------------------------------------------------------------------------
        |
        | You may wish to use controllers instead of, or in addition to, Closure
        | based routes. That's great! Here is an example controller method to
        | get you started. To route to this controller, just add the route:
        |
        |	Route::get('/', 'HomeController@showWelcome');
        |
         */

    public function showApplications()
    {
        if (Auth::check()) {
            $applications = DB::table('applications')->get();
            $enrollments = DB::table('enrollments')->where('user_id', Auth::user()->id)->get();
            $applications_by_auth_user = array();

            foreach ($enrollments as $enrollment) {
                foreach ($applications as $application) {
                    if ($enrollment->application_id == $application->id) {
                        $applications_by_auth_user[] = $application;
                    }
                }
            }
        }

        if (Auth::check()) {
            return View::make('extend_applications')->with('applications_by_auth_user', $applications_by_auth_user);
        } else {
            return View::make('homepage');
        }
    }

    public function createNewApplication()
    {
        return View::make('create_new_application');
    }

    public function generateNewApplication()
    {
        $input = Input::all();

        $rules = array(
            'application_name' => 'required',
            'password' => 'required',
            'confirm_password' => 'required',
            'confirm_password' => 'required|same:password',
            'username_for_application' => 'required'
        );

        $validation = Validator::make($input, $rules);
        $messages = $validation->messages();

        if ($validation->fails())
        {
            return Redirect::to('create_new_application')->withErrors($validation)->withInput();
        } else {

            // create a record in "applications" database
            $application = new Application;
            $application->name =  Crypt::encrypt($input['application_name']);
            $application->username = Crypt::encrypt($input['username_for_application']);
            $application->password = Crypt::encrypt($input['password']);
            if (isset($input['details'])) {
                $application->details = $input['details'];
            }
            $application->save();

            // create a record in "enrollments" database
            $enrollment = new Enrollment;
            $enrollment->user_id = Auth::user()->id;
            $enrollment->application_id = $application->id;
            $enrollment->save();

            return Redirect::to('applications');
        }
    }

    public function editApplications()
    {
        if (Auth::check()) {
            $applications = DB::table('applications')->get();
            $enrollments = DB::table('enrollments')->where('user_id', Auth::user()->id)->get();
            $applications_by_auth_user = array();

            foreach ($enrollments as $enrollment) {
                foreach ($applications as $application) {
                    if ($enrollment->application_id == $application->id) {
                        $applications_by_auth_user[] = $application;
                    }
                }
            }
        }

        if (Auth::check()) {
            return View::make('extend_edit_applications')->with('applications_by_auth_user', $applications_by_auth_user);
        } else {
            return View::make('homepage');
        }
    }

    public function deleteApplication($app_id_to_delete)
    {
        $application = Application::find($app_id_to_delete);
        $application->delete();

        $enrollment = DB::table('enrollments')->where('application_id', $app_id_to_delete);
        $enrollment->delete();

        return Redirect::to("edit_applications");
    }

    public function delSelApp()
    {
        $checked_app = Input::all();
        // $checked_app1 = array_shift($checked_app);


        foreach ($checked_app as $name => $app_id) {

            $application = Application::find($app_id);
            if ($application != null) {
                $application->delete();
                $enrollment = DB::table('enrollments')->where('application_id', $app_id);
                $enrollment->delete();
            }
        }

        return Redirect::to("edit_applications");
    }

    public function editApplication($app_id_to_edit)
    {
        $url = URL::to('edit_made_application');

        $application = Application::find($app_id_to_edit);
        return View::make("edit_made_application")->with('application', $application);
    }

    public function editSelApp(){
        $input = Input::all();
        // dd($input);
        $application = Application::find($input['application_id']);
        // dd($application);

        $application->name = Crypt::encrypt($input['application_name']);
        $application->username = Crypt::encrypt($input['username_for_application']);

        if ($input['password'] != null) {
            $application->password = Crypt::encrypt($input['password']);
        }

        $application->details = $input['details'];

        $application->save();

        return Redirect::to("edit_applications");

    }

}
